(function($){
	/*"use strict";*/
	$.entwine('ss', function($) {
		$('.ss-gridfield .col-buttons .action.gridfield-button-approve').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				if (confirm(ss.i18n._t('PaymentAction.APPROVE_CONFIRMATION', 'Are you sure you want to approve this payment?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Message">' + ss.i18n._t('PaymentAction.MESSAGE', 'Message') + '</label><input id="Message" class="text" type="text" /></div></div>';
					var actions = {};
					actions[ss.i18n._t('PaymentAction.APPROVE_PAYMENT', 'Approve Payment')] = function() {
		        		var message = $('#modal').find('#Message').val();
		        		
		          		$(this).dialog("close");
		          		
						// If the button is disabled, do nothing.
						if (self.button('option', 'disabled')) {
							e.preventDefault();
							return;
						}
						
						self.getGridField().reload(
							{data: [{name: self.attr('name'), value: self.val()}, {name: 'Message', value: message}]},
							function(){
								statusMessage(ss.i18n._t('PaymentAction.APPROVE_SUCCESS', 'Payment has been approved successfully'), 'good');
							}
						);

						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
					
					return false;
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
		
		$('#action_doApprovePayment').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				$('#Message').remove();
				if (confirm(ss.i18n._t('PaymentAction.APPROVE_CONFIRMATION', 'Are you sure you want to approve this payment?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Message">' + ss.i18n._t('PaymentAction.MESSAGE', 'Message') + '</label><input id="Message" class="text" type="text" /></div></div>';
					var actions = {};
					actions[ss.i18n._t('PaymentAction.APPROVE_PAYMENT', 'Approve Payment')] = function() {
		        		var message = $('#modal').find('#Message').val();
		        		
		        		self.parents('form').append('<input type="hidden" id="Message" name="Message" value="' + message + '" />');
		          		$(this).dialog("close");
		          		
		          		if(!self.is(':disabled')) {
							self.parents('form').trigger('submit', [self]);
						}
						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
					
					return false;
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
		
		$('.ss-gridfield .col-buttons .action.gridfield-button-decline').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				if (confirm(ss.i18n._t('PaymentAction.DECLINE_CONFIRMATION', 'Are you sure you want to decline this payment?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Message">' + ss.i18n._t('PaymentAction.MESSAGE', 'Message') + '</label><input id="Message" class="text" type="text" /></div></div>';
					var actions = {};
					actions[ss.i18n._t('PaymentAction.DECLINE_PAYMENT', 'Decline Payment')] = function() {
		        		var message = $('#modal').find('#Message').val();
		        		if(message == ''){
		        			alert(ss.i18n._t('PaymentAction.MESSAGE_REQUIRED', 'Message is required'));
		        			return false;
		        		}
		        		
		          		$(this).dialog("close");

						// If the button is disabled, do nothing.
						if (self.button('option', 'disabled')) {
							e.preventDefault();
							return;
						}
						
						self.getGridField().reload(
							{data: [{name: self.attr('name'), value: self.val()}, {name: 'Message', value: message}]},
							function(){
								statusMessage(ss.i18n._t('PaymentAction.DECLINE_SUCCESS', 'Payment has been declined successfully'), 'good');
							}
						);

						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
					
					return false;
				} else {
					e.preventDefault();
					return false;
				}
			}
		});

		$('#action_doDeclinePayment').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				$('#Message').remove();
				if (confirm(ss.i18n._t('PaymentAction.DECLINE_CONFIRMATION', 'Are you sure you want to decline this payment?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Message">' + ss.i18n._t('PaymentAction.MESSAGE', 'Message') + '</label><input id="Message" class="text" type="text" /></div></div>';
					var actions = {};
					actions[ss.i18n._t('PaymentAction.DECLINE_PAYMENT', 'Decline Payment')] = function() {
		        		var message = $('#modal').find('#Message').val();
		        		if(message == ''){
		        			alert(ss.i18n._t('PaymentAction.MESSAGE_REQUIRED', 'Message is required'));
		        			return false;
		        		}
		        		
		        		self.parents('form').append('<input type="hidden" id="Message" name="Message" value="' + message + '" />');
		          		$(this).dialog("close");
		          		
		          		if(!self.is(':disabled')) {
							self.parents('form').trigger('submit', [self]);
						}
						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
					
					return false;
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
		
		$('.ss-gridfield .col-buttons .action.gridfield-button-refund').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				if (confirm(ss.i18n._t('PaymentAction.REFUND_CONFIRMATION', 'Are you sure you want to refund this payment?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Message">' + ss.i18n._t('PaymentAction.MESSAGE', 'Message') + '</label><input id="Message" class="text" type="text" /></div></div>';
					var actions = {};
					actions[ss.i18n._t('PaymentAction.REFUND_PAYMENT', 'Refund Payment')] = function() {
		        		var message = $('#modal').find('#Message').val();
		        		if(message == ''){
		        			alert(ss.i18n._t('PaymentAction.MESSAGE_REQUIRED', 'Message is required'));
		        			return false;
		        		}
		        		
		          		$(this).dialog("close");

						// If the button is disabled, do nothing.
						if (self.button('option', 'disabled')) {
							e.preventDefault();
							return;
						}
						
						self.getGridField().reload(
							{data: [{name: self.attr('name'), value: self.val()}, {name: 'Message', value: message}]},
							function(){
								statusMessage(ss.i18n._t('PaymentAction.REFUND_SUCCESS', 'Payment has been refunded successfully'), 'good');
							}
						);
		
						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
					
					return false;
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
		
		$('#action_doRefundPayment').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				$('#Message').remove();
				if (confirm(ss.i18n._t('PaymentAction.REFUND_CONFIRMATION', 'Are you sure you want to refund this payment?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Message">' + ss.i18n._t('PaymentAction.MESSAGE', 'Message') + '</label><input id="Message" class="text" type="text" /></div></div>';
					var actions = {};
					actions[ss.i18n._t('PaymentAction.REFUND_PAYMENT', 'Refund Payment')] = function() {
		        		var message = $('#modal').find('#Message').val();
		        		if(message == ''){
		        			alert(ss.i18n._t('PaymentAction.MESSAGE_REQUIRED', 'Message is required'));
		        			return false;
		        		}
		        		
		        		self.parents('form').append('<input type="hidden" id="Message" name="Message" value="' + message + '" />');
		          		$(this).dialog("close");
		          		
		          		if(!self.is(':disabled')) {
							self.parents('form').trigger('submit', [self]);
						}
						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
					
					return false;
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
	});

}(jQuery));