if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('zh', {
		'PaymentAction.MESSAGE' : '信息',
		'PaymentAction.APPROVE_PAYMENT' : '批准此付款',
		'PaymentAction.APPROVE_CONFIRMATION' : '你确定要批准此付款？',
		'PaymentAction.DECLINE_CONFIRMATION' : '你确定要拒绝此付款？',
		'PaymentAction.REFUND_CONFIRMATION' : '你确定要退款此付款？',
		'PaymentAction.REFUND_PAYMENT' : '退还此付款',
		'PaymentAction.DECLINE_PAYMENT' : '拒绝此付款',
		'PaymentAction.MESSAGE_REQUIRED' : '信息为必填',
		'PaymentAction.APPROVE_SUCCESS' : '付款已批准成功',
		'PaymentAction.DECLINE_SUCCESS' : '付款已拒绝成功',
		'PaymentAction.REFUND_SUCCESS' : '付款已退款成功',
		'ReceiptAction.VOID_CONFIRMATION' : '你确定要废止此收据？',
		'ReceiptAction.REMARK' : '备注',
		'ReceiptAction.IS_REFUND' : '要退款？',
		'ReceiptAction.REMARK_REQUIRED' : '备注为必填',
		'ReceiptAction.VOID_RECEIPT' : '废止此收据',
		'ReceiptAction.VOID_SUCCESS' : '收据已废止成功'
	});
}