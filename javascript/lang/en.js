if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('en', {
		'PaymentAction.MESSAGE' : 'Message',
		'PaymentAction.APPROVE_PAYMENT' : 'Approve Payment',
		'PaymentAction.APPROVE_CONFIRMATION' : 'Are you sure you want to approve this payment?',
		'PaymentAction.DECLINE_CONFIRMATION' : 'Are you sure you want to decline this payment?',
		'PaymentAction.REFUND_CONFIRMATION' : 'Are you sure you want to refund this payment?',
		'PaymentAction.REFUND_PAYMENT' : 'Refund Payment',
		'PaymentAction.DECLINE_PAYMENT' : 'Decline Payment',
		'PaymentAction.MESSAGE_REQUIRED' : 'Message is required',
		'PaymentAction.APPROVE_SUCCESS' : 'Payment has been approved successfully',
		'PaymentAction.DECLINE_SUCCESS' : 'Payment has been declined successfully',
		'PaymentAction.REFUND_SUCCESS' : 'Payment has been refunded successfully',
		'ReceiptAction.VOID_CONFIRMATION' : 'Are you sure you want to void this receipt?',
		'ReceiptAction.REMARK' : 'Remark',
		'ReceiptAction.IS_REFUND' : 'Is Refund?',
		'ReceiptAction.REMARK_REQUIRED' : 'Remark is required',
		'ReceiptAction.VOID_RECEIPT' : 'Void Receipt',
		'ReceiptAction.VOID_SUCCESS' : 'Receipt has been voided successfully'
	});
}