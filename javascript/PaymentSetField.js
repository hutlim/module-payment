(function($) {
	$('select[rel~=paymentset]').each(function(index) {
		var paymentsetfield = $(this), paymentfields = paymentsetfield.parents('form').find('.composite.paymentfields');
		var payment = paymentsetfield.val();
		paymentfields.filter(function() {
		   return !$(this).hasClass(payment) || payment == '';
		}).addClass('hidden');
		paymentfields.filter(function() {
		   return payment && $(this).hasClass(payment);
		}).removeClass('hidden');
	});
	
	$('select[rel~=paymentset]').live('change', function() {
		var paymentsetfield = $(this), paymentset = paymentsetfield.parents('form').find('.field.paymentset'), paymentfields = paymentsetfield.parents('form').find('.composite.paymentfields');
		paymentset.find('span.message').remove();
		paymentfields.find('span.message').remove();
		var payment = this.value;
		paymentfields.filter(function() {
		   return !$(this).hasClass(payment) || payment == '';
		}).addClass('hidden');
		paymentfields.filter(function() {
		   return payment && $(this).hasClass(payment);
		}).removeClass('hidden');
	});
}(jQuery));