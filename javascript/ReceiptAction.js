(function($){
	/*"use strict";*/
	$.entwine('ss', function($) {
		$('#action_doPDF').entwine({
			onclick: function(e) {
				window.open($(this).data('pdf-link'), '_blank');
				e.preventDefault();
				return false;
			}
		});
		
		$('.ss-gridfield .col-buttons .action.gridfield-button-void').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				if (confirm(ss.i18n._t('ReceiptAction.VOID_CONFIRMATION', 'Are you sure you want to void this receipt?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Remark">' + ss.i18n._t('ReceiptAction.REMARK', 'Remark') + '</label><input id="Remark" class="text" type="text" /></div><div class="checkbox"><input id="IsRefund" class="checkbox" type="checkbox" /><label class="right" for="IsRefund">' + ss.i18n._t('ReceiptAction.IS_REFUND', 'Is Refund?') + '</label></div></div>';
					var actions = {};
					actions[ss.i18n._t('ReceiptAction.VOID_RECEIPT', 'Void Receipt')] = function() {
		        		var remark = $('#modal').find('#Remark').val();
		        		var refund = $('#modal').find('#IsRefund').is(':checked') ? 1 : 0;
		        		if(remark == ''){
		        			alert(ss.i18n._t('ReceiptAction.REMARK_REQUIRED', 'Remark is required'));
		        			return false;
		        		}
		        		
		          		$(this).dialog("close");

						// If the button is disabled, do nothing.
						if (self.button('option', 'disabled')) {
							e.preventDefault();
							return;
						}
						
						self.getGridField().reload(
							{data: [{name: self.attr('name'), value: self.val()}, {name: 'Remark', value: remark}, {name: 'IsRefund', value: refund}]},
							function(){
								statusMessage(ss.i18n._t('ReceiptAction.VOID_SUCCESS', 'Receipt has been voided successfully'), 'good');
							}
						);

						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
					
					return false;
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
		
		$('#action_doVoidReceipt').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				$('#Remark').remove();
				$('#IsRefund').remove();
				if (confirm(ss.i18n._t('ReceiptAction.VOID_CONFIRMATION', 'Are you sure you want to void this receipt?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Remark">' + ss.i18n._t('ReceiptAction.REMARK', 'Remark') + '</label><input id="Remark" class="text" type="text" /></div><div class="checkbox"><input id="IsRefund" class="checkbox" type="checkbox" /><label class="right" for="IsRefund">' + ss.i18n._t('ReceiptAction.IS_REFUND', 'Is Refund?') + '</label></div></div>';
					var actions = {};
					actions[ss.i18n._t('ReceiptAction.VOID_RECEIPT', 'Void Receipt')] = function() {
		        		var remark = $('#modal').find('#Remark').val();
		        		var refund = $('#modal').find('#IsRefund').is(':checked') ? 1 : 0;
		        		if(remark == ''){
		        			alert(ss.i18n._t('ReceiptAction.REMARK_REQUIRED', 'Remark is required'));
		        			return false;
		        		}
		        		
		        		self.parents('form').append('<input type="hidden" id="Remark" name="Remark" value="' + remark + '" /><input type="hidden" id="IsRefund" name="IsRefund" value="' + refund + '" />');
		          		$(this).dialog("close");
		          		
		          		if(!self.is(':disabled')) {
							self.parents('form').trigger('submit', [self]);
						}
						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
					
					return false;
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
	});

}(jQuery));