<% if $Format == 'PDF' %>
	<link rel="stylesheet" type="text/css" href="payment/css/ReceiptTemplate.css" />
<% else %>
	<link rel="stylesheet" type="text/css" href="/payment/css/ReceiptTemplate.css" />
<% end_if %>
<div id="payment-receipt">
    <div id="header"><%t ReceiptTemplate.ss.PATMENT_RECEIPT "Payment Receipt" %></div>
    <div id="identity">
        <div id="address">
            <p><strong>$SiteConfig.CompanyName</strong></p>
            <p>$SiteConfig.FullAddress</p>
            <% if $SiteConfig.CompanySupportEmail %>
            <p>$SiteConfig.CompanySupportEmail</p>
            <% end_if %>
            <% if $SiteConfig.CompanyPhone %>
            <p>$SiteConfig.CompanyPhone</p>
            <% end_if %>
        </div>
        <div id="logo">
            <% if $SiteConfig.CompanyLogo %>
                <img src="$SiteConfig.CompanyLogo.SetHeight(100).URL" alt="logo" />
            <% end_if %>
        </div>
    </div>
    <div style="clear:both"></div>
    <br />
    <div id="customer">
        <div id="customer-title">
            <p><strong><u><%t ReceiptTemplate.ss.BILLING_TO "Billing To" %></u></strong></p>
            <p>$Member.Name ($Member.Username)</p>
            <p>$Member.FullAddress</p>
            <% if $Member.Email %>
                <p>$Member.Email</p>
            <% end_if %>
            <% if $Member.Mobile %>
                <p>$Member.Mobile</p>
            <% end_if %>
        </div>
        <div id="meta">
	        <table>
	            <tr>
	                <td class="meta-head"><%t ReceiptTemplate.ss.PATMENT_REFERENCE "Payment Reference #" %></td>
	                <td>$Reference ($Status.Title)</td>
	            </tr>
	            <tr>
	                <td class="meta-head"><%t ReceiptTemplate.ss.DATE "Date" %></td>
	                <td>$Created.Nice</td>
	            </tr>
	            <% if $Remark %>
	                <tr>
	                    <td class="meta-head"><%t ReceiptTemplate.ss.REMARK "Remark" %></td>
	                    <td>$Remark</td>
	                </tr>
	            <% end_if %>
	            <tr>
	                <td class="meta-head"><%t ReceiptTemplate.ss.PRINTED_ON "Printed on" %></td>
	                <td>$PrintTime</td>
	            </tr>
	        </table>
		</div>
    </div>
    <div style="clear:both"></div>
    <br />
    <div id="items">
	    <table>
	        <tr>       
	            <td class="payment-method item-head"><%t ReceiptTemplate.ss.PATMENT_METHOD "Payment Method" %></td>
	            <td class="payment-detail item-head"><%t ReceiptTemplate.ss.DETAILS "Details" %></td>
	        </tr>
	        <% loop Payments %>
	            $FullDetailHTML
	        <% end_loop %>
	    </table>
    </div>
    <div style="clear:both"></div>
    <div id="terms">
        <p><%t ReceiptTemplate.ss.TERM "This is a computer generated statement, no company stamp or signature is required. We strongly encourages our customers to print this invoice out only when you need it. Save Trees, Save Earth." %></p>
    </div>
    <% if Format = HTML %>
        <div id="action">
        <% if PDFLink %>
            <a target="_blank" href="{$PDFLink}" title="<%t ReceiptTemplate.ss.DOWNLOAD_PDF "Download these details as PDF" %>"><img border="0" src="/payment/images/pdf.gif" alt="<%t ReceiptTemplate.ss.DOWNLOAD_PDF "Download these details as PDF" %>" /></a>
        <% end_if %>
        <% if PrintLink %>
            <a target="_blank" class="print" href="{$PrintLink}" title="<%t ReceiptTemplate.ss.PRINT "Print this page" %>"><img src="/payment/images/print.gif" border="0" alt="<%t ReceiptTemplate.ss.PRINT "Print this page" %>" /></a>
        <% end_if %>
        </div>
    <% end_if %>
</div>