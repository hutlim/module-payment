<?php
/**
 * @package  payment
 */

class ReceiptGridFieldDetailForm extends GridFieldDetailForm {
}

class ReceiptGridFieldDetailForm_ItemRequest extends GridFieldDetailForm_ItemRequest {
    private static $allowed_actions = array(
        'edit',
        'view',
        'ItemEditForm',
        'doPrint',
        'doPDF'
    );
    
    public function updateCMSActions() {
        $actions = FieldList::create();
		
		if($this->record->canPrint()){
			if($this->record->config()->allowed_pdf && class_exists('SS_DOMPDF')){
				$pdfButton = FormAction::create('doPDF', _t('ReceiptGridFieldDetailForm.BUTTONDOWNLOADPDF', 'Download PDF'))->setAttribute('data-icon', 'disk')->setAttribute('data-pdf-link', $this->Link('doPDF'))->setUseButtonTag(true);
				$actions->push($pdfButton);
			}
			
			$printButton = FormAction::create('doPrint', _t('ReceiptGridFieldDetailForm.BUTTONPRINT', 'Print'))->setAttribute('data-icon', 'grid_print')->setAttribute('data-print-link', $this->Link('doPrint'))->setUseButtonTag(true);
			$actions->push($printButton);
		}
        
        if($this->record->canVoid()){
            $voidButton = FormAction::create('doVoidReceipt', _t('ReceiptGridFieldDetailForm.BUTTONVOID', 'Void'))->setAttribute('data-icon', 'decline')->setUseButtonTag(true);
            $actions->push($voidButton);
        }
        
        return $actions;
    }

    public function ItemEditForm() {
    	Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('payment/javascript/lang');
		Requirements::javascript('payment/javascript/ReceiptAction.min.js');
		
        $form = parent::ItemEditForm();

        $form->setActions($this->updateCMSActions());
        return $form;
    }
    
    public function doVoidReceipt($data, $form) {
        $controller = $this->getToplevelController();
        if(!isset($data['Remark']) || $data['Remark'] == ''){
            $form->sessionMessage(_t('ReceiptGridFieldDetailForm.REMARK_REQUIRED', 'Remark is required'), 'bad');
            $responseNegotiator = new PjaxResponseNegotiator( array(
                'CurrentForm' => function() use (&$form) {
                    return $form->forTemplate();
                },
                'default' => function() use (&$controller) {
                    return $controller->redirectBack();
                }

            ));
            if($controller->getRequest()->isAjax()) {
                $controller->getRequest()->addHeader('X-Pjax', 'CurrentForm');
            }
            return $responseNegotiator->respond($controller->getRequest());
        }

        try {
            DB::getConn()->transactionStart();
			$refund = isset($data['IsRefund']) && $data['IsRefund'];
            Receipt::update_status($this->record->ID, 'Voided', $data['Remark'], $refund);
            DB::getConn()->transactionEnd();
            $this->record = DataObject::get_by_id($this->record->ClassName, $this->record->ID);
            $this->gridField->getList()->add($this->record);
        }
        catch(ValidationException $e) {
            DB::getConn()->transactionRollback();
            $form->sessionMessage($e->getResult()->message(), 'bad');
            $responseNegotiator = new PjaxResponseNegotiator( array(
                'CurrentForm' => function() use (&$form) {
                    return $form->forTemplate();
                },
                'default' => function() use (&$controller) {
                    return $controller->redirectBack();
                }

            ));
            if($controller->getRequest()->isAjax()) {
                $controller->getRequest()->addHeader('X-Pjax', 'CurrentForm');
            }
            return $responseNegotiator->respond($controller->getRequest());
        }

        $message = _t('ReceiptGridFieldDetailForm.SUCCESS_VOIDED_RECEIPT', 'Receipt has been successfully voided');
        //end custom code

        $form->sessionMessage($message, 'good');

        if($this->gridField->getList()->byId($this->record->ID)) {
            // Return new view, as we can't do a "virtual redirect" via the CMS Ajax
            // to the same URL (it assumes that its content is already current, and doesn't reload)
            return $this->edit($controller->getRequest());
        } else {
            // Changes to the record properties might've excluded the record from
            // a filtered list, so return back to the main view if it can't be found
            $noActionURL = $controller->removeAction($data['url']);
            $controller->getRequest()->addHeader('X-Pjax', 'Content');
            return $controller->redirect($noActionURL, 302);
        }
    }
    
    public function doPrint(){
        return $this->record->ViewPrint();
    }
	
	public function doPDF(){
		if($this->record->config()->allowed_pdf && class_exists('SS_DOMPDF')){
        	return $this->record->ViewPDF();
		}
    }
}
