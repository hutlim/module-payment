<?php
/**
 * Bulk action handler for void receipt
 * 
 * @package news
 */
class GridFieldBulkReceiptHandler extends GridFieldBulkHandler
{	
	/**
	 * RequestHandler allowed actions
	 * @var array
	 */
	private static $allowed_actions = array('void');


	/**
	 * RequestHandler url => action map
	 * @var array
	 */
	private static $url_handlers = array(
		'void' => 'void'
	);
	

	/**
	 * Void the selected records passed from the void bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of voided records ID
	 */
	public function void(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canVoid()){
					array_push($ids, $record->ID);
					Receipt::update_status($record->ID, 'Voided', '', true);
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkReceiptHandler.SUCCESS_VOIDED', 'Total {count} payment receipt has been voided', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}