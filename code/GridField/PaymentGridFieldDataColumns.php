<?php
/**
 * @package payment
 */
class PaymentGridFieldDataColumns implements GridField_ColumnProvider, GridField_ActionProvider {
    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
    	Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('payment/javascript/lang');
		Requirements::javascript('payment/javascript/PaymentAction.min.js');
    	Requirements::css('payment/css/PaymentGridFieldDataColumns.css');
    	$content = '';
		
		if($record->canRefund()){
    		$field = GridField_FormAction::create($gridField, 'Refund' . $record->ID, false, "refund", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-refund')->setAttribute('title', _t('PaymentGridFieldDataColumns.BUTTONREFUND', 'Refund'))->setAttribute('data-icon', 'arrow-circle-135-left')->setDescription(_t('PaymentGridFieldDataColumns.REFUND_PAYMENT', 'Refund Payment'));
			$content .= $field->Field();
		}
		
		if($record->canDecline()){
        	$field = GridField_FormAction::create($gridField, 'Decline' . $record->ID, false, "decline", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-decline')->setAttribute('title', _t('PaymentGridFieldDataColumns.BUTTONDECLINE', 'Decline'))->setAttribute('data-icon', 'cross-circle')->setDescription(_t('PaymentGridFieldDataColumns.DECLINE_PAYMENT', 'Decline Payment'));
			$content .= $field->Field();
		}
		
    	if($record->canApprove()){
    		$field = GridField_FormAction::create($gridField, 'Approve' . $record->ID, false, "approve", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-approve')->setAttribute('title', _t('PaymentGridFieldDataColumns.BUTTONAPPROVE', 'Approve'))->setAttribute('data-icon', 'accept')->setDescription(_t('PaymentGridFieldDataColumns.APPROVE_PAYMENT', 'Approve Payment'));
			$content .= $field->Field();
		}
		return $content;
    }

    public function getActions($gridField) {
        return array(
            'approve',
            'decline',
            'refund'
        );
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'approve' || $actionName == 'decline' || $actionName == 'refund') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if(!$item) {
                return;
            }
			
            if($actionName == 'approve') {
            	if(!$item->canApprove()) {
	                throw new ValidationException(_t('PaymentGridFieldDataColumns.APPROVE_PERMISSION', 'No permission to approve these item'), 0);
	            }
				
				try {
		            DB::getConn()->transactionStart();
	                $item->completePayment(array('Message' => isset($data['Message']) ? $data['Message'] : ''));
		            DB::getConn()->transactionEnd();
		        }
		        catch(ValidationException $e) {
		            DB::getConn()->transactionRollback();
		            throw new ValidationException($e->getMessage(), 0);
		        }
            }
			
            if($actionName == 'decline') {
            	if(!$item->canReject()) {
	                throw new ValidationException(_t('PaymentGridFieldDataColumns.DECLINE_PERMISSION', 'No permission to decline these item'), 0);
	            }
				elseif(!isset($data['Message']) || $data['Message'] == ''){
					throw new ValidationException(_t('PaymentGridFieldDataColumns.MESSAGE_REQUiRED', 'Message is required'), 0);
				}
				
				try {
		            DB::getConn()->transactionStart();
					$item->declinePayment(array('Message' => $data['Message']));
		            DB::getConn()->transactionEnd();
		        }
		        catch(ValidationException $e) {
		            DB::getConn()->transactionRollback();
		            throw new ValidationException($e->getMessage(), 0);
		        }
            }
			
			if($actionName == 'refund') {
            	if(!$item->canRefund()) {
	                throw new ValidationException(_t('PaymentGridFieldDataColumns.REFUND_PERMISSION', 'No permission to refund these item'), 0);
	            }
				elseif(!isset($data['Message']) || $data['Message'] == ''){
					throw new ValidationException(_t('PaymentGridFieldDataColumns.MESSAGE_REQUiRED', 'Message is required'), 0);
				}
				
				try {
		            DB::getConn()->transactionStart();
					$item->refundPayment(array('Message' => $data['Message']));
		            DB::getConn()->transactionEnd();
		        }
		        catch(ValidationException $e) {
		            DB::getConn()->transactionRollback();
		            throw new ValidationException($e->getMessage(), 0);
		        }
            }
        }
    }
}
