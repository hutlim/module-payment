<?php
/**
 * @package payment
 */
class ReceiptGridFieldDataColumns implements GridField_ColumnProvider, GridField_ActionProvider {
    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
    	Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('payment/javascript/lang');
		Requirements::javascript('payment/javascript/ReceiptAction.min.js');
    	Requirements::css('payment/css/ReceiptGridFieldDataColumns.css');
    	$content = '';
		
		if($record->canVoid()){
        	$field = GridField_FormAction::create($gridField, 'Void' . $record->ID, false, "void", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-void')->setAttribute('title', _t('ReceiptGridFieldDataColumns.BUTTONVOID', 'Void'))->setAttribute('data-icon', 'cross-circle')->setDescription(_t('ReceiptGridFieldDataColumns.VOID_RECEIPT', 'Void Receipt'));
			$content .= $field->Field();
		}
		return $content;
    }

    public function getActions($gridField) {
        return array(
            'void'
        );
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'void') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if(!$item) {
                return;
            }

        	if(!$item->canVoid()) {
                throw new ValidationException(_t('ReceiptGridFieldDataColumns.VOID_PERMISSION', 'No permission to void these item'), 0);
            }
			elseif(!$data['Remark']){
				throw new ValidationException(_t('ReceiptGridFieldDataColumns.REMARK_REQUiRED', 'Remark is required'), 0);
			}

			try {
	            DB::getConn()->transactionStart();
				$refund = isset($data['IsRefund']) && $data['IsRefund'];
	            Receipt::update_status($item->ID, 'Voided', $data['Remark'], $refund);
	            DB::getConn()->transactionEnd();
	        }
	        catch(ValidationException $e) {
	            DB::getConn()->transactionRollback();
	            throw new ValidationException($e->getMessage(), 0);
	        }
        }
    }
}
