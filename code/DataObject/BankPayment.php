<?php

Class BankPayment extends Payment {
    private static $singular_name = "Bank Payment";
    private static $plural_name = "Bank Payments";
    
    private static $db = array(
        'BankName' => 'Varchar(100)',
        'BranchName' => 'Varchar(100)',
        'AccountName' => 'Varchar(250)',
        'AccountNumber' => 'Varchar(100)',
        'TransactionID' => 'Varchar(100)',
        'TransactionDate' => 'Date',
        'TransactionTime' => 'Time'
    );
	
	private static $has_one = array(
		'TransactionReceipt' => 'TransactionReceipt'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BankName'] = _t('BankPayment.PAYOR_BANK_NAME', 'Payor Bank Name');
		$labels['BranchName'] = _t('BankPayment.PAYOR_BRANCH_NAME', 'Payor Branch Name');
		$labels['AccountName'] = _t('BankPayment.PAYOR_ACCOUNT_NAME', 'Payor Account Name');
		$labels['AccountNumber'] = _t('BankPayment.PAYOR_ACCOUNT_NUMBER', 'Payor Account No.');
		$labels['TransactionID'] = _t('BankPayment.TRANSACTION_ID', 'Transaction ID');
		$labels['TransactionDate'] = _t('BankPayment.TRANSACTION_DATE', 'Transaction Date');
		$labels['TransactionTime'] = _t('BankPayment.TRANSACTION_TIME', 'Transaction Time');
		$labels['TransactionReceipt'] = _t('BankPayment.TRANSACTION_RECEIPT', 'Transaction Receipt');
		$labels['TransactionReceiptID'] = _t('BankPayment.TRANSACTION_RECEIPT', 'Transaction Receipt');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->Status == 'Success'){
			$data = array(
				'BankName' => $this->BankName,
				'BranchName' => $this->BranchName,
				'AccountName' => $this->AccountName,
				'AccountNumber' => $this->AccountNumber,
				'TransactionID' => $this->TransactionID,
				'TransactionDate' => $this->TransactionDate,
				'TransactionTime' => $this->TransactionTime,
				'TransactionReceipt' => $this->TransactionReceiptID
			);
	        $validator = $this->getPaymentDetailsFormRequirements(BankWire_Validator::create(), $data);

			if($errors = $validator->getErrors()){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();
        
        $fields->makeFieldReadonly('BankName');
        $fields->makeFieldReadonly('BranchName');
        $fields->makeFieldReadonly('AccountName');
        $fields->makeFieldReadonly('AccountNumber');
        $fields->makeFieldReadonly('TransactionID');
        $fields->makeFieldReadonly('TransactionDate');
		$fields->makeFieldReadonly('TransactionTime');
		
		if($this->owner->exists()) {
			$transaction_receipt_link = $this->TransactionReceipt()->exists() ? sprintf('<a href="%s" target="_blank">%s</a>', $this->TransactionReceipt()->Link(), _t('BankPayment.DOWNLOAD_FILE', 'Download File')) : _t('BankPayment.FILE_NOT_UPLOAD', 'File not upload');
			$fields->replaceField('TransactionReceipt', HtmlEditorField_Readonly::create('TransactionReceiptLink', $this->fieldLabel('TransactionReceipt'), $transaction_receipt_link));
		}
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
    	$allowed_ext = array(
			'bmp',
            'jpg',
            'jpeg',
            'png',
            'gif',
            'tif',
            'tiff',
            'pdf',
            'doc',
            'docx',
            'xls',
            'xlsx'
        );
		
		$max_upload = (int)(ini_get('upload_max_filesize'));
		
        $fields = FieldList::create(
			TextField::create('BankPayment_BankName', $this->fieldLabel('BankName')),
			TextField::create('BankPayment_BranchName', $this->fieldLabel('BranchName')),
			TextField::create('BankPayment_AccountName', $this->fieldLabel('AccountName')),
			TextField::create('BankPayment_AccountNumber', $this->fieldLabel('AccountNumber')),
			TextField::create('BankPayment_TransactionID', $this->fieldLabel('TransactionID')),
            $date_field = DateField::create('BankPayment_TransactionDate', $this->fieldLabel('TransactionDate')),
            $time_field = TimeField::create('BankPayment_TransactionTime', $this->fieldLabel('TransactionTime')),
            $receipt_field = FileField::create('BankPayment_TransactionReceipt', $this->fieldLabel('TransactionReceipt'))
		);
		
		$receipt_field->getValidator()->setAllowedExtensions($allowed_ext);
		$receipt_field->getUpload()->setReplaceFile(false);
		$receipt_field->setFolderName(sprintf('BankReceipt/%s', Distributor::currentUserID()))->setDescription(sprintf('<p><strong class="text-danger">%s</strong></p><p><strong class="text-danger">%s</strong></p>', _t('BankPayment.FILE_SIZE_LIMIT', 'File Size Limit: {size}MB', '', array('size' => $max_upload)), _t('BankPayment.ALLWED_FILE_TYPE', 'Allowed File Type: {type}', '', array('type' => implode(', ', $allowed_ext)))));
		
		$site_config = SiteConfig::current_site_config();
		if($site_config->BankInstruction){
			$fields->insertBefore(HtmlEditorField_Readonly::create('BankPayment_BankInstruction', _t('BankPayment.BANK_INSTRUCTION', 'Bank Instruction'), $site_config->BankInstruction), 'BankPayment_BankName');
		}
		
		$date_field->setDescription(sprintf(
			_t('BankPayment.EXAMPLE', 'e.g. %s', 'Example format'),
			Convert::raw2xml(Zend_Date::now()->toString($date_field->getConfig('dateformat')))
		));
		
		$time_field->setDescription(sprintf(
			_t('BankPayment.EXAMPLE', 'e.g. %s', 'Example format'),
			Convert::raw2xml(Zend_Date::now()->toString($time_field->getConfig('timeformat')))
		));
        
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

	function getPaymentDetailsFormFields(){
		$allowed_ext = array(
			'bmp',
            'jpg',
            'jpeg',
            'png',
            'gif',
            'tif',
            'tiff',
            'pdf',
            'doc',
            'docx',
            'xls',
            'xlsx'
        );
		
		$max_upload = (int)(ini_get('upload_max_filesize'));
		
		$fields = FieldList::create(
			TextField::create('BankName', $this->fieldLabel('BankName')),
            TextField::create('BranchName', $this->fieldLabel('BranchName')),
            TextField::create('AccountName', $this->fieldLabel('AccountName')),
            TextField::create('AccountNumber', $this->fieldLabel('AccountNumber')),
            TextField::create('TransactionID', $this->fieldLabel('TransactionID')),
            $date_field = DateField::create('TransactionDate', $this->fieldLabel('TransactionDate')),
            $time_field = TimeField::create('TransactionTime', $this->fieldLabel('TransactionTime')),
            $receipt_field = FileField::create('TransactionReceipt', $this->fieldLabel('TransactionReceipt')),
            HiddenField::create('ID', 'ID')
        );
		
		$receipt_field->getValidator()->setAllowedExtensions($allowed_ext);
		$receipt_field->getUpload()->setReplaceFile(false);
		$receipt_field->setFolderName(sprintf('BankReceipt/%s', Distributor::currentUserID()))->setDescription(sprintf('<p><strong class="text-danger">%s</strong></p><p><strong class="text-danger">%s</strong></p>', _t('BankPayment.FILE_SIZE_LIMIT', 'File Size Limit: {size}MB', '', array('size' => $max_upload)), _t('BankPayment.ALLWED_FILE_TYPE', 'Allowed File Type: {type}', '', array('type' => implode(', ', $allowed_ext)))));
		
		$site_config = SiteConfig::current_site_config();
		if($site_config->BankInstruction){
			$fields->insertBefore(HtmlEditorField_Readonly::create('BankPayment_BankInstruction', _t('BankPayment.BANK_INSTRUCTION', 'Bank Instruction'), $site_config->BankInstruction), 'BankName');
		}
		
		$date_field->setDescription(sprintf(
			_t('BankPayment.EXAMPLE', 'e.g. %s', 'Example format'),
			Convert::raw2xml(Zend_Date::now()->toString($date_field->getConfig('dateformat')))
		));
		
		$time_field->setDescription(sprintf(
			_t('BankPayment.EXAMPLE', 'e.g. %s', 'Example format'),
			Convert::raw2xml(Zend_Date::now()->toString($time_field->getConfig('timeformat')))
		));
        
		$this->extend('updateBankPaymentDetailsFormFields', $fields);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['BankPayment_BankName'] == ''){
            $validator->validationError(
                'BankPayment_BankName',
                _t('BankPayment.PAYOR_BANK_NAME_REQUIRED', 'Payor bank name is required'),
                'required'
            );
        }
        
        if($data['BankPayment_AccountName'] == ''){
            $validator->validationError(
                'BankPayment_AccountName',
                _t('BankPayment.PAYOR_ACCOUNT_NAME_REQUIRED', 'Payor account name is required'),
                'required'
            );
        }
        
        if($data['BankPayment_AccountNumber'] == ''){
            $validator->validationError(
                'BankPayment_AccountNumber',
                _t('BankPayment.PAYOR_ACCOUNT_NUMBER_REQUIRED', 'Payor account number is required'),
                'required'
            );
        }

		$this->extend('updatePaymentFormRequirements', $validator, $data);
		
        return $validator;
    }

	function getPaymentDetailsFormRequirements($validator, $data){
        if($data['BankName'] == ''){
            $validator->validationError(
                'BankName',
                _t('BankPayment.PAYOR_BANK_NAME_REQUIRED', 'Payor bank name is required'),
                'required'
            );
        }
        
        if($data['AccountName'] == ''){
            $validator->validationError(
                'AccountName',
                _t('BankPayment.PAYOR_ACCOUNT_NAME_REQUIRED', 'Payor account name is required'),
                'required'
            );
        }
        
        if($data['AccountNumber'] == ''){
            $validator->validationError(
                'AccountNumber',
                _t('BankPayment.PAYOR_ACCOUNT_NUMBER_REQUIRED', 'Payor account number is required'),
                'required'
            );
        }
		
		if($data['TransactionID'] == ''){
            $validator->validationError(
                'TransactionID',
                _t('BankPayment.TRANSACTION_ID_REQUIRED', 'Transaction ID is required'),
                'required'
            );
        }
		
		if($data['TransactionDate'] == ''){
            $validator->validationError(
                'TransactionDate',
                _t('BankPayment.TRANSACTION_DATE_REQUIRED', 'Transaction date is required'),
                'required'
            );
        }

		if($data['TransactionTime'] == ''){
            $validator->validationError(
                'TransactionTime',
                _t('BankPayment.TRANSACTION_TIME_REQUIRED', 'Transaction time is required'),
                'required'
            );
        }
		
		if($data['TransactionReceipt'] == ''){
            $validator->validationError(
                'TransactionReceipt',
                _t('BankPayment.TRANSACTION_RECEIPT_REQUIRED', 'Transaction receipt is required'),
                'required'
            );
        }

		$this->extend('updatePaymentDetailsFormRequirements', $validator, $data);
		
        return $validator;
    }
	
	function canApprove($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
		
		if($this->exists() && ($this->Status == 'Incomplete' || $this->Status == 'Pending')){
			return Permission::check('APPROVE_Payment');
		}
        return false;
    }
    
    function pendingPayment($data = array()){
    	if($this->Status == 'Incomplete'){
        	$this->castedUpdate($data)->setField('Status', 'Pending')->write();
		}
        return $this;
    }
    
    function declinePayment($data = array()){
    	if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
        	$this->castedUpdate($data)->setField('Status', 'Declined')->write();
		}
        return $this;
    }
    
    function completePayment($data = array()){
    	if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
        	$this->castedUpdate($data)->setField('Status', 'Success')->write();
		}
        return $this;
    }
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('bankwire', 'process', $this->ID)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
    
    function getAction(){
        if($this->Status == 'Pending') {
            $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
            $link = $token->addToUrl(Controller::join_links('bankwire', 'payment_details', $this->ID));
            $action = '<a class="btn btn-warning btn-xs" title="'._t('BankPayment.CLICK_UPDATE_BANK_PAYMENT_DETAILS', 'Click here to update your bank payment details').'" data-title="'._t('BankPayment.UPDATE_BANK_PAYMENT_DETAILS', 'Update Bank Payment Details').'" rel="popup tooltip" data-modal-lg="1" href="' . $link . '">'._t('BankPayment.UPDATE_BANK_PAYMENT_DETAILS', 'Update Bank Payment Details').'</a>';
            return $action;
        }
    }
}

class BankWire_Validator extends RequiredFields {
	function __construct() {
		$required = func_get_args();
		if(isset($required[0]) && is_array($required[0])) {
			$required = $required[0];
		}
		
		parent::__construct($required);
	}
	
    function php($data) {
        $valid = parent::php($data);
		if(!$valid){
			return $valid;
		}
		
		singleton('BankPayment')->getPaymentDetailsFormRequirements($this, $data);
        $this->extend('updateBankWireValidator', $this, $data);
        return $this->getErrors();
    }

	function clearErrors(){
		$this->errors = null;
	}
}
?>