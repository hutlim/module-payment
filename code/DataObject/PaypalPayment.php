<?php

Class PaypalPayment extends Payment {
    private static $singular_name = "Paypal Payment";
    private static $plural_name = "Paypal Payments";
    
    private static $db = array(
        'AuthorisationCode' => 'Varchar(100)',
        'TxnRef' => 'Varchar(100)'
    );
    
    // URLs

    private static $url = 'https://www.paypal.com/cgi-bin/webscr';
    
    private static $test_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    
    static function set_url($url) {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.url" config setting instead');
        Config::inst()->update('PaypalPayment', 'url', $url);
    }
    
    static function get_url() {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.url" config setting instead');
        return Config::inst()->get('PaypalPayment', 'url');
    }
    
    static function set_test_url($test_url) {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.test_url" config setting instead');
        Config::inst()->update('PaypalPayment', 'test_url', $test_url);
    }
    
    static function get_test_url() {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.test_url" config setting instead');
        return Config::inst()->get('PaypalPayment', 'test_url');
    }

    // Test Mode

    private static $test_mode = false;
    
    private static $test_account_email;
    
    static function set_test_mode($test_account_email) {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.test_mode" config setting instead');
        Deprecation::notice('3.2', 'Use the "PaypalPayment.test_account_email" config setting instead');
        Config::inst()->update('PaypalPayment', 'test_mode', true);
        Config::inst()->update('PaypalPayment', 'test_account_email', $test_account_email);
    }
    
    static function get_test_mode() {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.test_mode" config setting instead');
        return Config::inst()->get('PaypalPayment', 'test_mode');
    }
    
    static function get_test_account_email() {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.test_account_email" config setting instead');
        return Config::inst()->get('PaypalPayment', 'test_account_email');
    }

    // Payment Informations

    private static $account_email;
    
    static function set_account_email($account_email) {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.account_email" config setting instead');
        Config::inst()->update('PaypalPayment', 'account_email', $account_email);
    }
    
    static function get_account_email() {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.account_email" config setting instead');
        return Config::inst()->get('PaypalPayment', 'account_email');
    }
    
    // Validate Type
    
    private static $validate_type = null; // PDT or IPN
    
    private static $pdt_token = null;
    
    static function set_pdt_validate($token) {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.validate_type" config setting instead');
        Deprecation::notice('3.2', 'Use the "PaypalPayment.pdt_token" config setting instead');
        Config::inst()->update('PaypalPayment', 'validate_type', 'PDT');
        Config::inst()->update('PaypalPayment', 'pdt_token', $token);
    }
    
    static function set_ipn_validate() {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.validate_type" config setting instead');
        Config::inst()->update('PaypalPayment', 'validate_type', 'IPN');
        Config::inst()->update('PaypalPayment', 'pdt_token', null);
    }
    
    static function get_validate_type() {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.validate_type" config setting instead');
        return Config::inst()->get('PaypalPayment', 'validate_type');
    }
    
    static function get_pdt_token() {
        Deprecation::notice('3.2', 'Use the "PaypalPayment.pdt_token" config setting instead');
        return Config::inst()->get('PaypalPayment', 'pdt_token');
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
        $logo = '<img src="payment/thirdparty/Paypal/paypal.jpg" alt="Credit card payments powered by PayPal"/>';
        $privacyLink = '<a href="https://www.paypal.com/us/cgi-bin/webscr?cmd=p/gen/ua/policy_privacy-outside" target="_blank" title="Read PayPal\'s privacy policy">' . $logo . '</a><br/>';
        $fields = new FieldList(
            new HtmlEditorField_Readonly('PayPalInfo', '', $privacyLink),
            new HtmlEditorField_Readonly(
                'PayPalPaymentsList',
                '',
                //TODO: these methods aren't available in all countries
                '<img src="payment/thirdparty/Paypal/methods/visa.jpg" alt="Visa"/>' .
                '<img src="payment/thirdparty/Paypal/methods/mastercard.jpg" alt="MasterCard"/>' .
                '<img src="payment/thirdparty/Paypal/methods/american-express.gif" alt="American Express"/>' .
                '<img src="payment/thirdparty/Paypal/methods/discover.jpg" alt="Discover"/>' .
                '<img src="payment/thirdparty/Paypal/methods/paypal.jpg" alt="PayPal"/>'
            )
        );
		
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
    	
		$this->extend('updatePaymentFormRequirements', $validator, $data);
		
        return $validator;
    }
    
    function cancelPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Canceled')->write();
        return $this;
    }
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function processPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Process')->write();
        return $this;
    }
    
    function declinePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Declined')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Success')->write();
        return $this;
    }
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('paypal', 'process', $this->ID)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
    
    function getAction(){
        if($this->Status == 'Incomplete' && $this->Receipt()->Status != 'Voided') {
            $action = '<a class="btn btn-warning btn-xs" title="'._t('PaypalPayment.CLICK_PROCEED_PAYMENT', 'Click here to proceed paypal payment').'" rel="tooltip" href="' . $this->ProcessLink() . '">'._t('PaypalPayment.PROCEED_PAYMENT', 'Proceed Paypal Payment').'</a>';
            
            $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
            $link = $token->addToUrl(Controller::join_links('paypal', 'pre_complete', $this->ID));
            $action .= '<a class="btn btn-warning btn-xs" title="'._t('PaypalPayment.CLICK_COMPLETED_PAYMENT', 'Click here if already completed paypal payment').'" data-title="'._t('PaypalPayment.PRECOMPLETED_PAYMENT', 'Pre-Completed Paypal Payment').'" rel="popup tooltip" href="' . $link . '">'._t('PaypalPayment.COMPLETED_PAYMENT', 'I\'ve Completed Paypal Payment').'</a>';
            return $action;
        }
    }
    
    function onBeforeWrite(){
        parent::onBeforeWrite();
        
        if($this->AuthorisationCode == ''){
            $this->AuthorisationCode = md5(uniqid(rand(), true));
        }
    }
}

Class PaypalPaymentLog extends DataObject {
    private static $db = array(
        'Title' => 'Varchar(250)',
        'Data' => 'Text'
    );
    
    private static $has_one = array('PaypalPayment' => 'PaypalPayment');
    
    static function create_log($payment_id, $title, $data){
        $log = new PaypalPaymentLog();
        $log->Title = $title;
        $log->Data = $data;
        $log->PaypalPaymentID = $payment_id;
        $log->write();
    }
}
?>