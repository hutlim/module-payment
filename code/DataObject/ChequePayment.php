<?php

Class ChequePayment extends Payment {
    private static $singular_name = "Cheque Payment";
    private static $plural_name = "Cheque Payments";
    
    private static $db = array(
        'ChequeName' => 'Varchar(100)',
        'ChequeReference' => 'Varchar(100)',
        'ChequeDate' => 'Date'
    );
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['ChequeName'] = _t('ChequePayment.CHEQUE_PAYEE', 'Cheque Payee');
		$labels['ChequeReference'] = _t('ChequePayment.CHEQUE_REFERENCE', 'Cheque Reference');
		$labels['ChequeDate'] = _t('ChequePayment.CHEQUE_DATE', 'Cheque Date');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'ChequePayment_ChequeName' => $this->ChequeName,
				'ChequePayment_ChequeReference' => $this->ChequeReference,
				'ChequePayment_ChequeDate' => $this->ChequeDate
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();
        
        $fields->makeFieldReadonly('ChequeName');
        $fields->makeFieldReadonly('ChequeReference');
        $fields->makeFieldReadonly('ChequeDate');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
        $fields = FieldList::create(
			TextField::create('ChequePayment_ChequeName', $this->fieldLabel('ChequeName')),
			TextField::create('ChequePayment_ChequeReference', $this->fieldLabel('ChequeReference')),
			DateField::create('ChequePayment_ChequeDate', $this->fieldLabel('ChequeDate'))
		);
		
		$site_config = SiteConfig::current_site_config();
		if($site_config->ChequeInstruction){
			$fields->insertBefore(HtmlEditorField_Readonly::create('ChequePayment_ChequeInstruction', _t('ChequePayment.CHEQUE_INSTRUCTION', 'Cheque Instruction'), $site_config->ChequeInstruction), 'ChequePayment_ChequeName');
		}

		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['ChequePayment_ChequeName'] == ''){
            $validator->validationError(
                'ChequePayment_ChequeName',
                _t('ChequePayment.CHEQUE_PAYEE_REQUIRED', 'Cheque payee is required'),
                'required'
            );
        }
        
        if($data['ChequePayment_ChequeReference'] == ''){
            $validator->validationError(
                'ChequePayment_ChequeReference',
                _t('ChequePayment.CHEQUE_REFERENCE_REQUIRED', 'Cheque reference is required'),
                'required'
            );
        }
        
        if($data['ChequePayment_ChequeDate'] == ''){
            $validator->validationError(
                'ChequePayment_ChequeDate',
                _t('ChequePayment.CHEQUE_DATE_REQUIRED', 'Cheque date is required'),
                'required'
            );
        }

		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }
	
	function canApprove($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
		
		if($this->exists() && ($this->Status == 'Incomplete' || $this->Status == 'Pending')){
			return Permission::check('APPROVE_Payment');
		}
        return false;
    }
    
    function pendingPayment($data = array()){
    	if($this->Status == 'Incomplete'){
        	$this->castedUpdate($data)->setField('Status', 'Pending')->write();
        }
        return $this;
    }
    
    function declinePayment($data = array()){
    	if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
        	$this->castedUpdate($data)->setField('Status', 'Declined')->write();
        }
        return $this;
    }
    
    function completePayment($data = array()){
    	if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
        	$this->castedUpdate($data)->setField('Status', 'Success')->write();
		}
        return $this;
    }
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('cheque', 'process', $this->ID)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
}

?>