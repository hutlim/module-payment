<?php
class PaymentStatusList extends DropdownList implements PermissionProvider {
    private static $singular_name = "Payment Status List";
    private static $plural_name = "Payment Status Lists";
    
    private static $default_records = array(
		array(
            'Code' => 'Incomplete',
            'Title' => 'Incomplete',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Success',
            'Title' => 'Success',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Failure',
            'Title' => 'Failure',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Declined',
            'Title' => 'Declined',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Canceled',
            'Title' => 'Canceled',
            'Sort' => 50,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Pending',
            'Title' => 'Pending',
            'Sort' => 60,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Refunded',
            'Title' => 'Refunded',
            'Sort' => 70,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Process',
            'Title' => 'Process',
            'Sort' => 80,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Incomplete',
            'Title' => '未完成',
            'Sort' => 90,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Success',
            'Title' => '付款成功',
            'Sort' => 100,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Failure',
            'Title' => '付款失败',
            'Sort' => 110,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Declined',
            'Title' => '付款终止',
            'Sort' => 120,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Canceled',
            'Title' => '付款取消',
            'Sort' => 130,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Pending',
            'Title' => '等待处理',
            'Sort' => 140,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Refunded',
            'Title' => '已退款',
            'Sort' => 150,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Process',
            'Title' => '正在处理',
            'Sort' => 160,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($kyc = PaymentStatusList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $kyc->Title;
        }
		return $code;
    }
}
?>
