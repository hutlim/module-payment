<?php
class ReceiptStatusList extends DropdownList implements PermissionProvider {
    private static $singular_name = "Receipt Status List";
    private static $plural_name = "Receipt Status Lists";
    
    private static $default_records = array(
        array(
            'Code' => 'Pending',
            'Title' => 'Pending',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Partially Paid',
            'Title' => 'Partially Paid',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Fully Paid',
            'Title' => 'Fully Paid',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Voided',
            'Title' => 'Voided',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Pending',
            'Title' => '等待处理',
            'Sort' => 50,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Partially Paid',
            'Title' => '部分付款',
            'Sort' => 60,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Fully Paid',
            'Title' => '付全款',
            'Sort' => 70,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Voided',
            'Title' => '已废止',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($kyc = ReceiptStatusList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $kyc->Title;
        }
		return $code;
    }
}
?>
