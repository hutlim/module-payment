<?php

Class TransactionType extends DataObject implements PermissionProvider {
    private static $singular_name = "Transaction Type";
    private static $plural_name = "Transaction Types";

    private static $db = array(
        'Code' => 'Varchar',
        'Title' => 'Varchar(100)',
        'Description' => 'Varchar(250)',
        'Object' => 'Varchar(100)',
        'Page' => 'Varchar(100)',
        'Action' => 'Varchar(100)'
    );

    static function get_id_by_code($code) {
        $transaction_type = TransactionType::get()->find('Code', $code);
        return $transaction_type ? $transaction_type->ID : 0;
    }

    static function get_title_by_code($code) {
        $transaction_type = TransactionType::get()->find('Code', $code);
        return $transaction_type ? $transaction_type->Title : '';
    }

    static function get_title_by_id($id) {
        $transaction_type = TransactionType::get()->byID($id);
        return $transaction_type ? $transaction_type->Title : '';
    }

    static function get_object_by_code($code) {
        $transaction_type = TransactionType::get()->find('Code', $code);
        return $transaction_type ? $transaction_type->Object : '';
    }

    static function get_object_by_id($id) {
        $transaction_type = TransactionType::get()->byID($id);
        return $transaction_type ? $transaction_type->Object : '';
    }

    function Link($receipt_id, $locale) {
        if($page = DataObject::get_one($this->Page)) {
            return Controller::join_links($page->Link($this->Action), $receipt_id, sprintf('?%s=%s', 'locale', $locale));
        }
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_TransactionType');
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('EDIT_TransactionType');
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('DELETE_TransactionType');
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_TransactionType');
    }

    public function providePermissions() {
        return array(
            'VIEW_TransactionType' => array(
                'name' => _t('TransactionType.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('TransactionType.PERMISSIONS_CATEGORY', 'Transaction Type')
            ),
            'EDIT_TransactionType' => array(
                'name' => _t('TransactionType.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => _t('TransactionType.PERMISSIONS_CATEGORY', 'Transaction Type')
            ),
            'DELETE_TransactionType' => array(
                'name' => _t('TransactionType.PERMISSION_DELETE', 'Allow delete access right'),
                'category' => _t('TransactionType.PERMISSIONS_CATEGORY', 'Transaction Type')
            ),
            'CREATE_TransactionType' => array(
                'name' => _t('TransactionType.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('TransactionType.PERMISSIONS_CATEGORY', 'Transaction Type')
            )
        );
    }
}
?>