<?php

class PaymentSetField extends DropdownField {    
    function __construct($name, $title = "", $source = array(), $value = "", $form = null) {
        Requirements::javascript(THIRDPARTY_DIR.'/jquery/jquery.js');
        Requirements::css('payment/css/PaymentSetField.css');
        Requirements::javascript('payment/javascript/PaymentSetField.min.js');
		$this->setEmptyString(_t('PaymentSetField.PLEASE_SELECT', 'Please select...'));
        parent::__construct($name, $title, $source, $value, $form);
    }
	
	function getAttributes() {
        return array_merge(
            parent::getAttributes(), array(
            	'rel' => 'paymentset'
            )
        );
    }
	
	function Type() {
		return 'paymentset dropdown';
	}
    
    public function validate($validator) {
        if(!$this->Value()) {
            $errorMessage = _t(
                'Form.FIELDISREQUIRED', 
                '{name} is required',
                array(
                    'name' => strip_tags(
                        '"' . ($this->Title() ? $this->Title() : $this->getName()) . '"'
                    )
                )
            );
            
            if($msg = $this->getCustomValidationMessage()) {
                $errorMessage = $msg;
            }
            $validator->validationError(
                $this->getName(),
                $errorMessage,
                "required"
            );
            return false;
        }

        return true;
    }
}
