<?php

class Bankwire extends Page_Controller {
    private static $allowed_actions = array(
        'process',
        'PaymentDetailsForm',
        'payment_details'
    );

    function process() {
    	$payment = BankPayment::get()->byID($this->request->param('ID'));
		if($payment){
			$token = new SecurityToken(sprintf('TOKEN_%s', $payment->Receipt()->Reference));
	        if($token->checkRequest($this->request)){
	        	try {
	            	DB::getConn()->transactionStart();
	            	$payment->pendingPayment();
					DB::getConn()->transactionEnd();
	            	return $this->redirect($payment->RedirectLink());
				}
	            catch(ValidationException $e) {
	                DB::getConn()->transactionRollback();
	                SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
					$this->setMessage('error', $e->getResult()->message());
					return $this->redirectBack();
	            }
	        }
		}
        
        return $this->httpError('404');
    }
    
    function PaymentDetailsForm() {
        $fields = singleton('BankPayment')->getPaymentDetailsFormFields();
        
        $actions = FieldList::create(FormAction::create("doUpdate", _t('Bankwire.BUTTONUPDATE', 'Update')));

        $validator = BankWire_Validator::create();
		
		$payment = BankPayment::get()->byID($this->request->param('ID'));
		if($payment && $payment->TransactionReceipt()->Name){
			$fields->dataFieldByName('TransactionReceipt')->setDescription(sprintf('<p><strong class="text-success">%s</strong></p>%s', _t('Bankwire.UPLOADED_FILE', 'Uploaded File: {file}', '', array('file' => $payment->TransactionReceipt()->Name)), $fields->dataFieldByName('TransactionReceipt')->getDescription()));
		}

        return Form::create($this, 'PaymentDetailsForm', $fields, $actions, $validator);
    }

    function doUpdate($data, $form) {
        try {
        	DB::getConn()->transactionStart();
            $payment = BankPayment::get()->byID((int)$data['ID']);
            if($payment->Status == 'Pending') {
                $form->saveInto($payment);
                $payment->write();
            }
			DB::getConn()->transactionEnd();
			$form->sessionMessage(_t('Bankwire.SUCCESS_UPDATE_BANK_DETAILS', 'You\'ve been update your bank payment details successfully'), 'success');
        }
        catch(ValidationException $e) {
        	DB::getConn()->transactionRollback();
            SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
            $form->sessionMessage($e->getResult()->message(), 'error');
        }
        
        $token = new SecurityToken(sprintf('TOKEN_%s', $payment->Receipt()->Reference));
        $link = $token->addToUrl(Controller::join_links('bankwire', 'payment_details', $data['ID']));
        return $this->redirect($link);
    }

    function payment_details() {
    	$payment = BankPayment::get()->byID($this->request->param('ID'));
		if($this->request->isAjax() && $payment && $payment->Status == 'Pending'){
			$token = new SecurityToken(sprintf('TOKEN_%s', $payment->Receipt()->Reference));
	        if($token->checkRequest($this->request)){
				return $this->PaymentDetailsForm()->loadDataFrom($payment)->forTemplate();
            }
        }

        return $this->httpError('404');
    }
}
