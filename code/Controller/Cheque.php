<?php

class Cheque extends Page_Controller {
    private static $allowed_actions = array(
        'process'
    );

    function process() {
    	$payment = ChequePayment::get()->byID($this->request->param('ID'));
		if($payment){
			$token = new SecurityToken(sprintf('TOKEN_%s', $payment->Receipt()->Reference));
	        if($token->checkRequest($this->request)){
	        	try {
	            	DB::getConn()->transactionStart();
	            	$payment->pendingPayment();
					DB::getConn()->transactionEnd();
	            	return $this->redirect($payment->RedirectLink());
				}
	            catch(ValidationException $e) {
	                DB::getConn()->transactionRollback();
	                SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
					$this->setMessage('error', $e->getResult()->message());
					return $this->redirectBack();
	            }
			}
        }
        
        return $this->httpError('404');
    }
}
