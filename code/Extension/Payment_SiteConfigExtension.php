<?php

class Payment_SiteConfigExtension extends DataExtension {
    private static $db = array(
        'BankInstruction' => 'HTMLText',
        'ChequeInstruction' => 'HTMLText'
    );
	
	function updateFieldLabels(&$labels) {
		$labels['BankInstruction'] = _t('Payment_SiteConfigExtension.BANK_INSTRUCTION', 'Bank Instruction');
		$labels['ChequeInstruction'] = _t('Payment_SiteConfigExtension.CHEQUE_INSTRUCTION', 'Cheque Instruction');
    }

    function updateCMSFields(FieldList $fields) {
    	$fields->findOrMakeTab('Root.BankInstruction', _t('Payment_SiteConfigExtension.BANK_INSTRUCTION', 'Bank Instruction'));
        $fields->addFieldToTab('Root.BankInstruction', HtmlEditorField::create('BankInstruction', $this->owner->fieldLabel('BankInstruction')));
		
		$fields->findOrMakeTab('Root.ChequeInstruction', _t('Payment_SiteConfigExtension.CHEQUE_INSTRUCTION', 'Cheque Instruction'));
        $fields->addFieldToTab('Root.ChequeInstruction', HtmlEditorField::create('ChequeInstruction', $this->owner->fieldLabel('ChequeInstruction')));
    }
}
