<?php
/**
 * Payment administration interface, based on ModelAdmin
 * @package payment
 */
class PaymentAdmin extends GeneralModelAdmin {

    private static $url_segment = 'payment';
    private static $menu_title = 'Payment Receipt';
    private static $menu_icon = 'payment/images/payment-icon.png';

    private static $managed_models = array(
        'Receipt'
    );
    
    public function init() {
        parent::init();
        LiveVersioned::reading_stage('Live');   
    }
    
    public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
        if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
        if($this->modelClass == 'Receipt'){
            $listField = GridField::create(
                $this->sanitiseClassName($this->modelClass),
                false,
                $list,
                $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                    ->removeComponentsByType('GridFieldDetailForm')
                    ->removeComponentsByType('GridFieldAddNewButton')
                    ->removeComponentsByType('GridFieldDeleteAction')
                    ->addComponents(new ReceiptGridFieldDetailForm(), new ReceiptGridFieldDataColumns(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
            );
			
			if(Permission::check('VOID_Receipt')){
				$fieldConfig->addComponent($bulkButton = new GridFieldBulkAction(), 'GridFieldSortableHeader');
				$bulkButton->addBulkAction('void', _t('PaymentAdmin.VOID', 'Void'), 'GridFieldBulkReceiptHandler');
			}
        }
        else{
            $listField = GridField::create(
                $this->sanitiseClassName($this->modelClass),
                false,
                $list,
                $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                    ->removeComponentsByType('GridFieldFilterHeader')
                    ->addComponents(new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
            );
        }

        // Validation
        if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
            $detailValidator = singleton($this->modelClass)->getCMSValidator();
			if($this->modelClass == 'Receipt'){
				$listField->getConfig()->getComponentByType('ReceiptGridFieldDetailForm')->setValidator($detailValidator);
			}
			else{
            	$listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
			}
        }

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
?>